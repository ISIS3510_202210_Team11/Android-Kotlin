@file:Suppress("DEPRECATION", "UNUSED_PARAMETER")

package com.uniandes.swapp.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.uniandes.swapp.R
import com.uniandes.swapp.databinding.ActivityPersonProfileBinding
import com.uniandes.swapp.model.Person
import com.uniandes.swapp.viewModel.PersonProfileViewModel

class PersonProfile : AppCompatActivity() {
    private lateinit var analytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        analytics = Firebase.analytics
        setContentView(R.layout.activity_person_profile)
        val viewModel = ViewModelProviders.of(this).get(PersonProfileViewModel::class.java)
        var userName = intent.getStringExtra("userName")
        if (userName != null) {
            viewModel.loadPerson(userName)
        }
        val binding: ActivityPersonProfileBinding = DataBindingUtil.setContentView(this, R.layout.activity_person_profile)
        binding.vm = viewModel
        binding.lifecycleOwner = this
        viewModel.person.observe(this, Observer {
            var trees: TextView = findViewById(R.id.txt_trees)
            trees.text = "Trees planted: " + (viewModel.person.value?.treesPlanted ?: "")
            var waste: TextView = findViewById(R.id.txt_waste)
            waste.text = "Textile waste saved: " + (viewModel.person.value?.textileWasteSaved ?: "") + " kg"
        })
    }

    fun newProduct(view: View) {
        startActivity(Intent(this, NewProduct::class.java))
    }

    fun posts(view: View) {
        // TODO
    }

    fun settings(view: View) {
        // TODO
    }

    fun navigate(view: View) {
        // TODO
    }
}