package com.uniandes.swapp.viewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import com.uniandes.swapp.model.Person

class PersonProfileViewModel: ViewModel() {
    var person = MutableLiveData<Person?>()

    fun loadPerson(userName: String)
    {
        val db = Firebase.firestore
        val user = db.collection("Person").document(userName)
        user.get().addOnSuccessListener {documentSnapshot ->
            val data = documentSnapshot.toObject<Person>()
            person.setValue(data)
        }
    }
}