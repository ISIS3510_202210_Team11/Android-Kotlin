package com.uniandes.swapp.model.data

import androidx.room.*
import java.util.*

@Dao
    interface TransactionDao {
        @Query("SELECT * FROM transaction")
        fun getAll(): List<Transaction>

        @Query("SELECT * FROM transaction WHERE uid IN (:transactionsIds)")
        fun loadAllByIds(transactionsIds: IntArray): List<Transaction>

        @Query("SELECT * FROM transaction WHERE date LIKE :date AND " +
                "status LIKE :status LIMIT 1")
        fun findByName(date: Date, status: String): Transaction

        @Insert
        fun insertAll(vararg transactions: Transaction)

        @Delete
        fun delete(transactions: Transaction)
    }
