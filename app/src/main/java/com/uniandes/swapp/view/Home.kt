package com.uniandes.swapp.view

import android.content.Context
import android.content.pm.PackageManager
import android.location.LocationManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.location.Location
import android.Manifest
import android.content.Intent
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import android.widget.ImageButton
import android.util.Log
import android.view.View
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.uniandes.swapp.R
import java.util.*
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager

class Home : AppCompatActivity(), SensorEventListener{
    private lateinit var analytics: FirebaseAnalytics
    private lateinit var currentLocation: Location
    private lateinit var fusedLocationProvider: FusedLocationProviderClient
    private val permissionCode = 101

    private lateinit var sensorManager: SensorManager
    private var temperature: Sensor? = null
    private var available: Boolean = false
    private var mssg: String = " "

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        analytics = Firebase.analytics

        fusedLocationProvider = LocationServices.getFusedLocationProviderClient(this)

        val buttonLocation: ImageButton = findViewById(R.id.imageButton20)
        //val textLocation: TextView = findViewById(R.id.textView)

        buttonLocation.setOnClickListener {
            Log.d("Debug:", CheckPermission().toString())
            Log.d("Debug:", isLocationEnabled().toString())

            fetchLocation()
        }

        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager

        if (sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE) != null) {
            temperature = sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE)
            available = true
        } else {
            available = false
        }

    }

    private fun fetchLocation(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
            PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
            PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), permissionCode)
            return
        }

        val task = fusedLocationProvider.lastLocation
        task.addOnSuccessListener { location ->
            if (location != null){
                currentLocation = location
                Toast.makeText(this, currentLocation.latitude.toString() + "" + currentLocation.longitude, Toast.LENGTH_SHORT).show()

            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode){
            permissionCode -> if (grantResults.isEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                fetchLocation()
            }
        }
    }

    fun isLocationEnabled(): Boolean {
        //this function will return to us the state of the location service
        //if the gps or the network provider is enabled then it will return true otherwise it will return false

        var locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    fun CheckPermission(): Boolean {
        //this function will return a boolean
        //true: if we have permission
        //false if not
        if (
            ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED ||
            ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }

        return false

    }

    fun personProfile(view: View) {
        val intent = Intent(this, PersonProfile::class.java).apply {
            putExtra("userName", "maria.linares@example.com")
        }
        startActivity(intent)
    }

    override fun onSensorChanged(event: SensorEvent) {
        //Enviar notificación con recomendación
        if (event.values[0] > -30.0 && event.values[0] <= -10.0) {
            mssg = "Check out a coat or some gloves for the cold weather!"
            Toast.makeText(
                this,
                mssg,
                Toast.LENGTH_LONG
            ).show()
        } else if (event.values[0] > -10.0 && event.values[0] <= 15.0) {
            mssg = "You should check out sweaters and hats!"
            Toast.makeText(this, mssg, Toast.LENGTH_LONG).show()
        } else if (event.values[0] > 15.0 && event.values[0] <= 25.0) {
            mssg = "Feels like spring! Check out a cool light jacket!"
            Toast.makeText(
                this,
                mssg,
                Toast.LENGTH_LONG
            ).show()
        } else if (event.values[0] > 25.0) {
            mssg = "Great weather today! Go get some sun and maybe a pair of shorts!"
            Toast.makeText(
                this,
                mssg,
                Toast.LENGTH_LONG
            ).show()
        }
    }

    override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {
        Toast.makeText(this, "Accuracy change", Toast.LENGTH_SHORT).show()
    }

    override fun onResume() {
        // Register a listener for the sensor.
        super.onResume()
        if (available) {
            sensorManager.registerListener(this, temperature, SensorManager.SENSOR_DELAY_NORMAL)
        }
    }

    override fun onPause() {
        // Be sure to unregister the sensor when the activity pauses.
        super.onPause()
        if (available) {
            sensorManager.unregisterListener(this)
        }
    }

}

