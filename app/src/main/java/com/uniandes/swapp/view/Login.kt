package com.uniandes.swapp.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.uniandes.swapp.R

class Login : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var analytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        analytics = Firebase.analytics
        auth = Firebase.auth

        val login_button = findViewById<Button>(R.id.login_button)

        login_button.setOnClickListener{
            val email = findViewById<EditText>(R.id.email_text).text.toString()
            val password = findViewById<EditText>(R.id.password_text).text.toString()

            when{
                email.isEmpty() || password.isEmpty() -> {
                    Toast.makeText(baseContext, "Incorrect email or password.",
                        Toast.LENGTH_SHORT).show()
                } else -> {
                    Login(email, password)
                }
            }
        }
    }

    public override fun onStart(){
        super.onStart()
        val currentUser = auth.currentUser
        if (currentUser != null){
            reload()
        }
    }

    private fun Login (email : String, password: String){
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("TAG", "signInWithEmail:success")
                    reload()
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("TAG", "signInWithEmail:failure", task.exception)
                    Toast.makeText(baseContext, "Incorrect email or password.",
                        Toast.LENGTH_SHORT).show()
                }
            }
    }

    private fun reload(){
        val intent = Intent (this@Login, Home::class.java)
        this.startActivity(intent)
    }
}