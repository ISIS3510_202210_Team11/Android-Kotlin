package com.uniandes.swapp.model

data class Product(
    var id: Long? = null,
    var size: String? = null,
    var color: String? = null,
    var name: String? = null,
    var brand: String? = null,
    var category: String? = null,
    var isSwappable: Boolean? = null,
    var suggestedPrice: Double? = null,
    var state: String? = null,
    var tags: MutableList<String>? = null
)
