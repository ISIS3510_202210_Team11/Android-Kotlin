package com.uniandes.swapp.view

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.uniandes.swapp.R

class NewProduct : AppCompatActivity() {

    private lateinit var analytics: FirebaseAnalytics

    companion object{
        private const val CAMERA_PERMISSION_CODE = 1
        private const val CAMERA_REQUEST_CODE = 2
        private const val GALLERY_PERMISSION_CODE = 100
        private const val GALLERY_REQUEST_CODE = 200
    }

    private val launcher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){ activityResult ->
        if (activityResult.resultCode == RESULT_OK){
            val thumbNail: Bitmap = activityResult.data!!.extras!!.get("data") as Bitmap
            val imageView = findViewById<ImageView>(R.id.chosen_image)
            imageView.setImageBitmap(thumbNail)
        }
    }

    private val galleryLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){ activityResult ->
        if (activityResult.resultCode == RESULT_OK){
            val imageView = findViewById<ImageView>(R.id.chosen_image)
            imageView.setImageURI(activityResult.data?.data)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_product)

        analytics = Firebase.analytics

        //Camera feature
        val cameraButton = findViewById<Button>(R.id.photo_button)
        cameraButton.setOnClickListener {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.CAMERA
                ) == PackageManager.PERMISSION_GRANTED
            ){
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                launcher.launch(intent)
            }else{
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.CAMERA),
                    CAMERA_PERMISSION_CODE
                )
            }
        }
        //Gallery feature
        val galleryButton = findViewById<Button>(R.id.gallery_button)
        galleryButton.setOnClickListener {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) == PackageManager.PERMISSION_GRANTED
            ){
                val intentGallery = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
                //val intentGallery = Intent(Intent.ACTION_PICK)
                intentGallery.type = "image/*"
                galleryLauncher.launch(intentGallery)
            }else{
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    GALLERY_PERMISSION_CODE
                )
            }
        }
    }

    //Permissions
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == CAMERA_REQUEST_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                launcher.launch(intent)
            } else {
                Toast.makeText(
                    this,
                    "Access to camera denied.",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        else if (requestCode == GALLERY_REQUEST_CODE){
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val intentGallery = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
                //val intentGallery = Intent(Intent.ACTION_PICK)
                intentGallery.type = "image/*"
                galleryLauncher.launch(intentGallery)
            } else {
                Toast.makeText(
                    this,
                    "Access to gallery denied.",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    fun personProfile(view: View) {
        this.onBackPressed()
    }
}