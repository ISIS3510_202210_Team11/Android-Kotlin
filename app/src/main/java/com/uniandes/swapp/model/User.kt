package com.uniandes.swapp.model

abstract class User {
    abstract var userName: String?
    abstract var name: String?
    abstract var password: String?
    abstract var type: String?
    abstract var profilePicture: String?
    abstract var address: MutableList<String>?
    abstract var city: String?
    abstract var averageRating: Int?
    abstract var posts: MutableList<Product>?
    abstract var reviews: MutableList<Review>?
    abstract var transactions: MutableList<Transaction>?
}