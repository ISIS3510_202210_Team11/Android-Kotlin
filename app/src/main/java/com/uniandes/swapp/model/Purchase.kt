package com.uniandes.swapp.model

data class Purchase(
    override var id: Long? = null,
    override var date: String? = null,
    override var checkedByUser1: Boolean? = null,
    override var checkedByUser2: Boolean? = null,
    override var status: String? = null,
    override var user1: User? = null,
    override var user2: User? = null,
    var amount: Double? = null,
    var paymentMethod: String? = null,
    var items: MutableList<Product>? = null
): Transaction()
