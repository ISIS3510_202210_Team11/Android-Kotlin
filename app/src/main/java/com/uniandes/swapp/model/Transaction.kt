package com.uniandes.swapp.model

abstract class Transaction {
    abstract var id: Long?
    abstract var date: String?
    abstract var checkedByUser1: Boolean?
    abstract var checkedByUser2: Boolean?
    abstract var status: String?
    abstract var user1: User?
    abstract var user2: User?
}