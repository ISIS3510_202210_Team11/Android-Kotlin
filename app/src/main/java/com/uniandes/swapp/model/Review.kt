package com.uniandes.swapp.model

data class Review(
    var rating: Int? = null,
    var comment: String? = null
)
