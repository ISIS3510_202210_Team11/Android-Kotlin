package com.uniandes.swapp.model

data class Swap(
    override var id: Long? = null,
    override var date: String? = null,
    override var checkedByUser1: Boolean? = null,
    override var checkedByUser2: Boolean? = null,
    override var status: String? = null,
    override var user1: User? = null,
    override var user2: User? = null,
    var user1Deposit: Boolean? = null,
    var user2Deposit: Boolean? = null,
    var productUser1: Product? = null,
    var productUser2: Product? = null
): Transaction()