package com.uniandes.swapp.model

data class Business(
    override var userName: String? = null,
    override var name: String? = null,
    override var password: String? = null,
    override var type: String? = null,
    override var profilePicture: String? = null,
    override var address: MutableList<String>? = null,
    override var city: String? = null,
    override var averageRating: Int? = null,
    override var posts: MutableList<Product>? = null,
    override var reviews: MutableList<Review>? = null,
    override var transactions: MutableList<Transaction>? = null,
    var nit: String? = null,
    var website: String? = null,
    var category: String? = null,
    var description: String? = null
): User()
